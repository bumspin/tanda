// JavaScript Document


(function(){
	"use strict";
	
	var sidebar = $(".sidebar"),
		mainbody = $(".mainbody"),
		sidebarDrawer = $(".sidebar-drawer"),
		windowWidth = $(window).innerWidth(),
		tabLink = $(".tabLink"),
		count = 0;
	
	sidebarDrawer.on('click', function(){
		if(count === 0){
			sidebar.addClass("sidebar-slide");
			mainbody.addClass("mainbody-slide");
			setTimeout(function(){ tabLink.show(); }, 300);
			count = 1;
		} else {
			sidebar.removeClass("sidebar-slide");
			mainbody.removeClass("mainbody-slide");
			tabLink.hide();
			count = 0;
		}
	});
	
}());